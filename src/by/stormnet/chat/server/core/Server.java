package by.stormnet.chat.server.core;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private ServerSocket serverSocket;
	private boolean isRunning;
	
	public Server(int portNumber) throws IOException {
		this.serverSocket = new ServerSocket(portNumber);
	}
	
	public void start() throws Exception {
		if (this.serverSocket == null){
			throw new Exception("server socket is null");
		}
		
		System.out.println("starting server on port: " + this.serverSocket.getLocalPort());
		this.isRunning = true;
		while (this.isRunning){
			Socket clientSocket = this.serverSocket.accept();
			ClientThread client = new ClientThread(clientSocket);
			
		}
	}

}
