package by.stormnet.chat.server.core;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import by.stormnet.chat.server.interfaces.IMessageDispatcherDelegate;

public class ClientThread extends Thread {
	private Socket socket;
	private InputStream inStream;
	private OutputStream outStream;
	private IMessageDispatcherDelegate msgDispatcherDelegate;
	
	private String clientId;
	private boolean isRunning;
	
	public ClientThread(Socket socket) {
		this.socket = socket;
	}

	public IMessageDispatcherDelegate getMsgDispatcherDelegate() {
		return msgDispatcherDelegate;
	}

	public void setMsgDispatcherDelegate(IMessageDispatcherDelegate msgDispatcherDelegate) {
		this.msgDispatcherDelegate = msgDispatcherDelegate;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	
	@Override
	public void run() {
		this.isRunning = true;
		
	}
}
