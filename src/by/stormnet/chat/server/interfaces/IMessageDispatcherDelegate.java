package by.stormnet.chat.server.interfaces;

public interface IMessageDispatcherDelegate {
	void processIncomingMessage(String msgSource);
}
